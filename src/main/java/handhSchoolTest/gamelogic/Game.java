package handhSchoolTest.gamelogic;

import handhSchoolTest.gameplayers.Monster;
import handhSchoolTest.gameplayers.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {

    public void startGame() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите имя Игрока. Или нажмите enter.");
        String line = reader.readLine();

        Player player;
        if (line.isBlank()) {
            player = new Player();
        } else {
            player = new Player(line);
        }

        System.out.println("Статистика игрока:");
        System.out.println("Здоровье игрока: " + player.getHealth());
        System.out.println("Атака игрока: " + player.getAttack());
        System.out.println("Защита игрока: " + player.getDefense());
        System.out.println("Урон игрока: " + player.getDamage());

        System.out.println();

        Monster monster = new Monster();
        System.out.println("Статистика монстра:");
        System.out.println("Здоровье монстра: " + monster.getHealth());
        System.out.println("Атака монстра: " + monster.getAttack());
        System.out.println("Защита монстра: " + monster.getDefense());
        System.out.println("Урон монстра: " + monster.getDamage());

        GameMove move = new GameMove();

        while (player.getHealth() > 0 && monster.getHealth() > 0) {
            System.out.println();
            System.out.println("Здоровье на данном этапе: " + player.getHealth());
            System.out.println("Нажмите Y чтобы атаковать или H для исцеления");
            String line2 = reader.readLine();
            if (line2.equalsIgnoreCase("Y")) {
                move.gameMove(player, monster);
                if (monster.getHealth() >= 0) {
                    move.gameMove(monster, player);
                }
            } else if (line2.equalsIgnoreCase("H")) {
                player.healing();
            } else {
                System.out.println("Введите Y или N.");
            }
        }
        if (monster.getHealth() <= 0) {
            System.out.println();
            System.out.println(player.getName() + " победил!!!");
            System.out.println("Отличная игра!");
        } else {
            System.out.println();
            System.out.println("Игра завершена победой монстра..");
        }
    }


}


