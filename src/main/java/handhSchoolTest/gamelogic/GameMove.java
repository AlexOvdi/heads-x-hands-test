package handhSchoolTest.gamelogic;

import handhSchoolTest.gameplayers.CharacterOfGame;

import java.util.Random;

public class GameMove {

    private Random random = new Random();

    public void gameMove(CharacterOfGame attacker, CharacterOfGame defender) {

        System.out.println(attacker.getName() + " решил атаковать!");
        int attackModifier = attacker.getAttack() - defender.getDefense() + 1;

        if (attackModifier <= 0) {
            attackModifier = 1;
        }

        System.out.println("Модификатор атаки " + attacker.getName() + ": " + attackModifier);
        Dice dice = new Dice();

        for (int i = 1; i < attackModifier + 1 && i <= 6; i++) {
            System.out.println("Бросок кубика №" + i);

            int diceRandom = dice.getRandomDiceNumber();

            if (diceRandom > 4) {
                System.out.println("Успешное попадание!");
                int moveDamage = random.nextInt(attacker.getDamage()) + 1;
                defender.setHealth(defender.getHealth() - moveDamage);
                System.out.println(defender.getName() + " получил " + moveDamage + " единицы урона.");
                System.out.println("Здоровье " + defender.getName() + ": " + defender.getHealth());
                System.out.println("---");
                break;
            } else if (diceRandom <= 4 && i < attackModifier) {
                System.out.println("Промах! Бросайте кубик еще раз.");

            } else {
                System.out.println("Тотальная неудача.. Ход переходит другому игроку.");
            }
        }
    }
}
