package handhSchoolTest.controller;

import handhSchoolTest.gamelogic.Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("Начать играть? Y/N");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        while (true) {
            String line = reader.readLine();
            if (line.equalsIgnoreCase("Y")) {
                Game game = new Game();
                game.startGame();
                break;
            } else if (line.equalsIgnoreCase("N")) {
                System.out.println("Goodbye!");
                break;
            } else {
                System.out.println("Введите Y или N.");
            }
        }
    }






}


