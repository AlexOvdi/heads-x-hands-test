package handhSchoolTest.gameplayers;

import java.util.Arrays;
import java.util.List;

public class Monster extends CharacterOfGame {

    private final static List<String> monsterNames = Arrays.asList(
            "Злой монстр",
            "Сильный злой монстр",
            "Устрашающий монстр",
            "Уставший джо",
            "Безобидный Васян"
    );

    public Monster() {
        this.name = getRandomName();
    }

     private String name;

    private int health = generateParameter(100) + 1;

    private final int defense = generateParameter(30) + 1;

    private final int attack = generateParameter(30) + 1;


    private final int damage = generateParameter(6) + 1;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public int getDefense() {
        return defense;
    }

    @Override
    public int getAttack() {
        return attack;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    private String getRandomName() {
        return monsterNames.get((int) (Math.random() * monsterNames.size()));
    }

}


