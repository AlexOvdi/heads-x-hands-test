package handhSchoolTest.gameplayers;


import java.util.Random;

public abstract class CharacterOfGame {

    private final Random random = new Random();

    private String name;
    private int health;
    private int defense;
    private int attack;
    private int damage;

    public int generateParameter(int bound) {
        return random.nextInt(bound) + 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
