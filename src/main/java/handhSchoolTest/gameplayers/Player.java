package handhSchoolTest.gameplayers;


public class Player extends CharacterOfGame {

    public Player() {this.name = "Игрок";}

    public Player(String name) {this.name = name;}

    private String name;
    private int health = generateParameter(100) + 1;
    private final int fullHealth = health;
    private final int defense = generateParameter(30) + 1;
    private final int attack = generateParameter(30) + 1;
    private final int damage = generateParameter(6) + 1;
    private int healingScore = 4;

    public void healing() {

        if (healingScore > 0 && health < fullHealth) {
            int afterHealingHealth = (int) ((0.3 * fullHealth + health) % 1);
            health = Math.max(afterHealingHealth, fullHealth);
            System.out.println("Исцеление прошло успешно. Уровень здоровья: " + health);
            healingScore--;
        } else if (health == fullHealth) {
            System.out.println("Максимальное здоровье.");
        } else if (healingScore == 0) {
            System.out.println("Исцеления закончились.");
        }


    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public void setHealth(int health) {
        this.health = health;
    }

    public int getFullHealth() {
        return fullHealth;
    }

    @Override
    public int getDefense() {
        return defense;
    }

    @Override
    public int getAttack() {
        return attack;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    public int getHealingScore() {
        return healingScore;
    }

    public void setHealingScore(int healingScore) {
        this.healingScore = healingScore;
    }


}
